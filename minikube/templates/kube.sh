#!/usr/bin/env bash
set -e
set -u
set -o pipefail

DNSMASQ_PATH=/etc/NetworkManager/dnsmasq.d/kube.conf
NC='\033[0m'

function messageOK() {
  echo -e "\033[0;92m✓ $1${NC}"
}

function messageAction() {
  echo -e "\033[0;96m⚑ $1"
}

function messageEnd() {
  echo -e "${NC}"
}

function messageError() {
  echo -e "\033[0;91m✗ $1${NC}"
}

function confirm() {
  echo -e "\033[0;95m→ $1 ? [y/N]${NC}"
  read ans
  if [ "${ans:-N}" != "y" ]; then
    messageError "Cancelled by user. Exiting"
    exit 1
  fi
}

## Redirect to minikube
# See https://askubuntu.com/questions/1029882/how-can-i-set-up-local-wildcard-127-0-0-1-domain-resolution-on-18-04?rq=1
function dnsmasq_redirect() {
  messageAction "Need sudo right to update ${DNSMASQ_PATH} and restart network manager"
  echo "${masq_content}" | sudo tee "${DNSMASQ_PATH}" >/dev/null
  sudo systemctl reload NetworkManager
  messageOK "Redirection set up"
}

function dnsmasq_setup() {
  masq_content="cache-size=0
address=/kube/$(minikube ip | head -1)"
  messageAction "Set up dnsmasq.d : ${masq_content}"
  if [ -f "${DNSMASQ_PATH}" ]; then
    current=$(cat "${DNSMASQ_PATH}")
    [ "${current}" != "${masq_content}" ] && dnsmasq_redirect
  else
    dnsmasq_redirect
  fi
  messageOK "dnsmasq set up : ${DNSMASQ_PATH}"
}

function kubectl() {
  IMAGE='registry.gitlab.com/savadenn-public/kubetools'
  docker pull "${IMAGE}" || true
  args=()
  args+=('run')
  args+=('--rm')
  args+=('-it')
  args+=('--network=host')
  args+=('--env')
  args+=('MINIKUBE_IP='"$(minikube ip)")
  args+=('--volume')
  args+=("${HOME}/.cache/helm:/root/.cache/helm")
  args+=('--volume')
  args+=("${HOME}/.kube/config:/root/.kube/config")
  args+=('--volume')
  args+=("${HOME}/.minikube:${HOME}/.minikube")
  args+=('--volume')
  args+=('{{ minikube.paths.files }}:/kube:ro')

  set +u
  [[ -n "${MINIKUBE_DOCKER_PROJECT}" ]] && args+=('--volume')
  [[ -n "${MINIKUBE_DOCKER_PROJECT}" ]] && args+=("${MINIKUBE_DOCKER_PROJECT}"':/project')
  set -u

  args+=("${IMAGE}")
  messageAction "Launching docker"
  messageAction "   docker ${args[*]} $*"
  docker "${args[@]}" "$@"
}

function printHelp() {
  messageAction "$(basename "$0") COMMAND"
  echo "  Runs commands for your local minikube cluster"
  echo ""
  echo "           : start kubectl connected to cluster (start it if needed) "
  echo "             you may specified /project mount with MINIKUBE_DOCKER_PROJECT env"
  echo "             MINIKUBE_DOCKER_PROJECT='/my/path' $(basename "$0")"
  echo "  help     : print this help"
  echo "  start    : start local cluster (create it if needed)"
  echo "  install  : Install base application"
  echo "  status   : eq. minikube status"
  echo "  stop     : stop cluster"
  echo "  delete   : destroy local cluster"
  echo "  recreate : delete + start + install"
}

function start() {
  messageAction "Starting cluster"
  minikube start --driver=docker
  dnsmasq_setup
  messageOK "Cluster started"
}

function install() {
  messageAction "Checking basic configuration"
  minikube addons enable metrics-server
  kubectl -c 'helmfile --file /kube/helmfile.yaml apply'
  messageOK "  Cluster ready"
}

function delete() {
  confirm "You're going to delete your minikube and all its data. Are you sure"

  messageAction "Deleting cluster"
  minikube delete
  messageOK "Cluster deleted"
}

function status() {
  messageAction "Kube status"
  minikube status
  return $?
}

#
# Main
#
if [[ $# == 0 ]]; then
  status || start
  kubectl
elif [[ $# -gt 1 ]]; then
  printHelp
elif [[ "$1" == "help" ]]; then
  printHelp
elif [[ "$1" == "start" ]]; then
  start
elif [[ "$1" == "install" ]]; then
  install
elif [[ "$1" == "status" ]]; then
  status
elif [[ "$1" == "stop" ]]; then
  minikube stop
elif [[ "$1" == "delete" ]]; then
  delete
elif [[ "$1" == "recreate" ]]; then
  delete
  start
  install
else
  printHelp
fi
