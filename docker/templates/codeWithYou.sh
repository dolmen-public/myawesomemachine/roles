#!/bin/bash

set -e
set -u

NC='\033[0m'

function messageOK() {
  echo -e "\033[0;92m✓ $1${NC}"
}

function messageAction() {
  echo -e "\033[0;96m⚑ $1"
}

function messageEnd() {
  echo -e "${NC}"
}

messageAction "Ask for a port forward on 443 to the host of COdeWithMe"
messageAction "What is the port shown in your CodeWithMe instance ?"
read -r port

messageEnd "Stopping your traefik"
docker-compose -f {{ docker.paths.files }}/docker-compose.yml stop traefik
messageAction "[Sudo required] Starting binding of ${port} to 443"

trap after SIGINT

function after {
  messageOK "Stopped"
  messageAction "Restarting your traefik"
  messageEnd
  localhost
}

# Sudo here to wait for user approval
sudo echo ""

messageOK "Running port forward"
messageAction "CTRL+C to stop"
messageEnd
sudo socat tcp-l:443,fork,reuseaddr tcp:127.0.0.1:"${port}"
