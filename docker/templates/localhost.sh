#!/bin/bash

# Default the first argument to "start" if it's not provided
arg1=${1:-start}

set -e
set -u

NC='\033[0m'

############################################################
# Functions                                                #
############################################################
function messageOK() {
  echo -e "\033[0;92m✓ $1${NC}"
}

function messageAction() {
  echo -e "\033[0;96m⚑ $1"
}


function start()
{
  docker-compose -f {{ docker.paths.files }}/docker-compose.yml pull || true
  docker-compose -f {{ docker.paths.files }}/docker-compose.yml up -d --force-recreate
  messageOK 'Server started'
  echo 'GUI availables'
  echo '  - Traefik  : https://localhost'
}

function stop()
{
  docker-compose -f {{ docker.paths.files }}/docker-compose.yml down
  messageOK 'Server stopped'
}


############################################################
# Main                                                     #
############################################################
case "$arg1" in
  start)
    start
	  exit;;
  stop)
	  stop
	  exit;;
  *) # Display Help
    echo "Command loadbalancer for local development with docker"
    echo
    echo "Syntax: localhost [start|stop]"
    echo "argument:"
    echo "  start    (default if no options)"
    echo "  stop"
    echo
	  exit;;
esac

