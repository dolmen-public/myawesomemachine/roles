#!/usr/bin/env sh
set -e
set -u

# Switch to Mumble channel AFK if running
afk() {
	if [ $(pidof mumble) ]; then
		mumble mumble://@{{ mumble.host }}:{{ mumble.port }}/{{ mumble.channel }} > /dev/null 2>&1
	fi
}

dbus-monitor --session "type='signal',interface='org.cinnamon.ScreenSaver'" |
  while read x; do
    case "$x" in
      *"boolean true"*) afk;;
    esac
  done
