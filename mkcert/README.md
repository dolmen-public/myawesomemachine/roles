# mkcert role

Generate self-signed root certificate and install it in your browser to have trusted local certificates.

## Manual procedure

1. Install required library
  ```shell
  sudo apt update && sudo apt install -y libnss3-tools
  ```
2. Get latest released from [mkcert](https://github.com/FiloSottile/mkcert/releases)
  ```shell
  # Exemple for 1.4.3 for linux64
  sudo wget https://github.com/FiloSottile/mkcert/releases/download/v1.4.3/mkcert-v1.4.3-linux-amd64 -O /usr/local/bin/mkcert
  sudo chmod 755 /usr/local/bin/mkcert
  
  # Init folder 
  sudo mkdir -p /usr/local/etc/mkcert
  
  # Remove all CA if exist
  sudo rm -rf /usr/local/etc/mkcert/*.pem || true
  
  # Generate new CA
  CAROOT=/usr/local/etc/mkcert
  export CAROOT
  sudo mkcert -uninstall
  sudo mkcert -install
  
  # Install CA for user (temp change access rights)
  sudo chmod 755 ${CAROOT}/rootCA-key.pem
  mkcert -install
  sudo chmod 400 ${CAROOT}/rootCA-key.pem
  ```