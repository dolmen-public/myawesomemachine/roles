#!/bin/sh

# This script, when called, locks session if yubikey is absent

sleep 2

if lsusb | grep "{{ yubilock.idVendor }}:" >> /dev/null 2>&1
then
  echo "Key still in, skip lock screen"
else
  loginctl lock-sessions
fi